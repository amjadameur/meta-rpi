# Base this image on core-image-base
inherit core-image
inherit extrausers

EXTRA_USERS_PARAMS = "usermod -P root root;"

# Include modules in rootfs
IMAGE_INSTALL += " \
	kernel-modules \
	"

IMAGE_FEATURES += "ssh-server-openssh debug-tweaks"

IMAGE_INSTALL_append = " \
        packagegroup-core-boot \
	openssh-sftp-server \
	init-ip-raspberrypi \	
	"

SDIMG_ROOTFS_TYPE = "ext4"
