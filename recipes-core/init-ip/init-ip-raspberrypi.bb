LICENSE = "CLOSED"
PR = "1"

SRC_URI = "file://set_rpi_static_ip"

inherit update-rc.d
INITSCRIPT_PACKAGES = "${PN}"
S = "${WORKDIR}"
INITSCRIPT_NAME = "set_rpi_static_ip"

do_install() {
	install -d ${D}${INIT_D_DIR}
	install -m 0755 ${S}/${INITSCRIPT_NAME} ${D}${INIT_D_DIR}/${INITSCRIPT_NAME}
}

FILES_${PN} += "${INIT_D_DIR}/set_rpi_static_ip"
